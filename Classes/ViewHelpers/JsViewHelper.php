<?php
namespace RZ\Fancyboxcontent\ViewHelpers;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * @author Raphael Zschorsch <rafu1987@gmail.com>
 * @package Fancyboxcontent
 * @subpackage ViewHelpers
 */
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class JsViewHelper extends AbstractViewHelper
{

    /**
     * Add JS
     *
     * @param string $uid The uid
     * @param string $settings The settings
     * @param string $labels The labels
     * @return string
     */
    public function render($uid, $settings, $labels)
    {
        // Dependency: fancyboxcontent - check if razor is installed
        if (ExtensionManagementUtility::isLoaded('razor')) {
            $dependencies = 'razorMainJs';
        }

        \FluidTYPO3\Vhs\Asset::createFromSettings([
            'name' => 'fancyboxcontent-' . $uid,
            'path' => 'typo3conf/ext/fancyboxcontent/Resources/Public/Js/fancyboxcontent.js',
            'fluid' => 1,
            'dependencies' => $dependencies,
            'variables' => [
                'uid' => $uid,
                'speed' => $settings['speed'],
                'loop' => $settings['loop'],
                'margin' => $settings['margin'],
                'opacity' => $settings['opacity'],
                'gutter' => $settings['gutter'],
                'infobar' => $settings['infobar'],
                'smallBtn' => $settings['smallBtn'],
                'touch' => $settings['touch'],
                'keyboard' => $settings['keyboard'],
                'focus' => $settings['focus'],
                'buttons' => $settings['buttons'],
                'slideShow' => $settings['slideShow'],
                'fullScreen' => $settings['fullScreen'],
                'thumbs' => $settings['thumbs'],
                'closeBtn' => $settings['closeBtn'],
                'lang' => $settings['lang'],
            ],
        ]);
    }

}
