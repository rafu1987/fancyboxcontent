<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "fancyboxcontent".
 *
 * Auto generated 15-01-2018 11:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'fancyBox content',
    'description' => 'Inserts a fancybox plugin for content',
    'category' => 'plugin',
    'author' => 'Raphael Zschorsch',
    'author_email' => 'rafu1987@gmail.com',
    'state' => 'beta',
    'uploadfolder' => false,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '3.1.2',
    'constraints' => [
        'depends' => [
            'extbase' => '8.7.0-8.7.99',
            'fluid' => '8.7.0-8.7.99',
            'typo3' => '8.7.0-8.7.99',
            'php' => '7.0.0-7.2.99',
            'vhs' => '',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'RZ\\Fancyboxcontent\\' => 'Classes',
        ],
    ],
    'clearcacheonload' => false,
    'author_company' => null,
];
